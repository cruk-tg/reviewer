package uk.ac.ed.ecmc.reviewer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.gui2.dialogs.MessageDialog;
import com.googlecode.lanterna.gui2.dialogs.MessageDialogButton;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.ed.ecmc.reviewer.dialogs.LoadFileBundleDialog;
import uk.ac.ed.ecmc.reviewer.files.ReviewerManifestFile;
import uk.ac.ed.ecmc.reviewer.ui.ApplicationEventHandler;
import uk.ac.ed.ecmc.reviewer.ui.MainApplicationScreen;

public class Application {
	private final MainApplicationScreen applicationScreen = MainApplicationScreen.getInstance();
    private final ApplicationEventHandler applicationEventHandler = new ApplicationEventHandler(this);
    private ReviewerManifestFile curReviewerManifestFile;
    private boolean showingMedCatAnnoations = true;
    private static Logger logger = LogManager.getLogger(Application.class);

    public Application(String[] args) {

    }

    public void start() {

    }

    public MainApplicationScreen getApplicationScreen() {
        return applicationScreen;
    }

    public ApplicationEventHandler getApplicationEventHandler() {
        return applicationEventHandler;
    }

    public void openFileBundle() {
        LoadFileBundleDialog dialog = new LoadFileBundleDialog();						
        
        File file = dialog.showDialog(applicationScreen.getTextGUI());
        if (file == null)
            return;

        try {
            curReviewerManifestFile = ReviewerManifestFile.readFile(file);
        
            applicationScreen.setReport(curReviewerManifestFile.getPathologyReviewerFile().getTokens());
            applicationScreen.setNLP("MedCAT Annotations", curReviewerManifestFile.getMedCatReviewerFile());

        } catch (FileNotFoundException e) {
            logger.error("Unable to find pathology file");
            
            MessageDialog
                .showMessageDialog(applicationScreen.getTextGUI(), "Load error", "Unable to find pathology file in bundle", MessageDialogButton.OK);
        }
    }

    public void cycleActiveWindow() {
        WindowBasedTextGUI textGUI = applicationScreen.getTextGUI();
        textGUI.cycleActiveWindow(false);
        try {
            textGUI.updateScreen();
        } catch (IOException e) {
            logger.fatal("An IOException occured switch windows {}", e.getMessage());
            for(StackTraceElement ste: e.getStackTrace()) {
                logger.fatal(ste.toString());
            }
            System.exit(1);
        }
    }

    public void switchAnnotations() {
        if (curReviewerManifestFile != null) {
            if (showingMedCatAnnoations) {
                applicationScreen.setNLP("cTAKES Annotations", curReviewerManifestFile.getCtakesReviewerFile());				
            } else {
                applicationScreen.setNLP("MedCAT Annotations", curReviewerManifestFile.getMedCatReviewerFile());
            }
            showingMedCatAnnoations = !showingMedCatAnnoations;
        }
    }
}
