package uk.ac.ed.ecmc.reviewer.ui.components;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.ThemeDefinition;
import com.googlecode.lanterna.graphics.ThemeStyle;
import com.googlecode.lanterna.gui2.AbstractListBox;
import com.googlecode.lanterna.gui2.ComponentRenderer;
import com.googlecode.lanterna.gui2.TextGUIGraphics;

public class SpaceComponent extends AbstractReportBlockComponent<SpaceComponent> {
	ThemeDefinition themeDefinition = getTheme().getDefinition(AbstractListBox.class);

	public SpaceComponent() {
		this.setSize(new TerminalSize(1, 1));
	}
	
	@Override
	protected ComponentRenderer<SpaceComponent> createDefaultRenderer() {
		return new ComponentRenderer<SpaceComponent>() {

			@Override
			public TerminalSize getPreferredSize(SpaceComponent component) {
				return new TerminalSize(1, 1);
			}

			@Override
			public void drawComponent(TextGUIGraphics graphics, SpaceComponent component) {
				ThemeStyle style = isHighlighted ? themeDefinition.getSelected() : themeDefinition.getNormal();
				graphics.applyThemeStyle(style);
				graphics.putString(0, 0, " ");			
			}		
		};
	}
}
