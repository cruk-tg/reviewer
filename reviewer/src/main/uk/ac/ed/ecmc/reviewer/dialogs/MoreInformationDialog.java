package uk.ac.ed.ecmc.reviewer.dialogs;

import java.util.Arrays;

import com.googlecode.lanterna.gui2.Button;
import com.googlecode.lanterna.gui2.Direction;
import com.googlecode.lanterna.gui2.GridLayout;
import com.googlecode.lanterna.gui2.LinearLayout;
import com.googlecode.lanterna.gui2.Panel;
import com.googlecode.lanterna.gui2.Panels;
import com.googlecode.lanterna.gui2.Separator;
import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.gui2.dialogs.DialogWindow;

public class MoreInformationDialog extends DialogWindow {
    private final Panel labelsPanel = new Panel(new LinearLayout(Direction.VERTICAL));
    private final Panel valuesPanel  = new Panel(new LinearLayout(Direction.VERTICAL));
    
    public MoreInformationDialog(String title) {
        super(title);
        Panel contentPanel = new Panel(new GridLayout(2));
        Panel rhsPanel = new Panel(new LinearLayout(Direction.VERTICAL));    
        
        valuesPanel.addTo(rhsPanel);
        

        labelsPanel.addTo(contentPanel);
        rhsPanel.addTo(contentPanel);
        new Separator(Direction.HORIZONTAL)
                .setLayoutData(
                        GridLayout.createLayoutData(
                                GridLayout.Alignment.FILL,
                                GridLayout.Alignment.CENTER,
                                true,
                                false,
                                2,
                                1))
                .addTo(contentPanel);

        Panels.grid(1, closeButton())
            .setLayoutData(GridLayout.createLayoutData(GridLayout.Alignment.END, GridLayout.Alignment.CENTER, false, false, 2, 1))
            .addTo(contentPanel);
        setComponent(contentPanel);
        setHints(Arrays.asList(Hint.CENTERED, Hint.MODAL));
    }
    
    @Override
    public Object showDialog(WindowBasedTextGUI textGUI) {
        super.showDialog(textGUI);
        return null;
    }

    public void addSnomed(String s) {
        
    }

    protected Panel getValuesPanel() {
        return valuesPanel;
    }

    protected Panel getLabelsPanel() {
        return labelsPanel;
    }

    private Button closeButton() {
        return new Button("Close", new CloseDialogAction());
    }

    private class CloseDialogAction implements Runnable {

        @Override
        public void run() {
            close();
        }

    }
}
