package uk.ac.ed.ecmc.reviewer.annotations;

public class AdditionalItem {
    private final String label;
    private final String value;

    public AdditionalItem(String label, String value) {
        this.label = label;
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;
    }
}
