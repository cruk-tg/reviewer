package uk.ac.ed.ecmc.reviewer.ctakes;

public interface IIdable extends Comparable<IIdable> {
    public Integer getId();
    public String getType();

    default int compareTo(IIdable o) {
        if (this.getType().compareTo(o.getType()) == 0) {
            return this.getId().compareTo(o.getId());
        }
        return this.getType().compareTo(o.getType());
    }

}
