package uk.ac.ed.ecmc.reviewer.annotations;

import uk.ac.ed.ecmc.reviewer.ctakes.AbstractCtakesMention;
import uk.ac.ed.ecmc.reviewer.ctakes.CtakesSet;
import uk.ac.ed.ecmc.reviewer.ctakes.UmlsConcept;
import uk.ac.ed.ecmc.reviewer.dialogs.MoreInformationDialogBuilder;
import uk.ac.ed.ecmc.reviewer.ui.MainApplicationScreen;

public class CtakesRunnableAnnotation extends RunnableAnnotation {
    public CtakesRunnableAnnotation(AbstractCtakesMention annotation, CtakesSet set) {
        super(annotation);
        
    }

    @Override
    public void run() {
        AbstractCtakesMention acAnnotation = (AbstractCtakesMention)annotation;

		MoreInformationDialogBuilder builder = new MoreInformationDialogBuilder()
			.setStart(acAnnotation.getStart())
			.setEnd(acAnnotation.getEnd())
			.setType(acAnnotation.getType())
            .setAcc(acAnnotation.getConfidence())
            .setPrettyName(acAnnotation.getSubject());
        if (acAnnotation.getUmlsConcept() != null)  {
            UmlsConcept uc = acAnnotation.getUmlsConcept();
            builder.setAcc(uc.getScore()).setCui(uc.getCui()).setPrettyName(uc.getPreferredText())
                .setTui(uc.getTui());
            if (uc.getCodingScheme().equalsIgnoreCase("SNOMEDCT_US")) {
                builder.setSnomedCodes(new String[] { uc.getCode() });
            }            
        }
        builder.build().showDialog(MainApplicationScreen.getInstance().getTextGUI());
    }

    @Override
    public String toString() {
        AbstractCtakesMention a = (AbstractCtakesMention)annotation;
        return String.format("%s [%d,%d] (%s)", a.getType(), a.getStart(), a.getEnd(), a.getSubject());
    }
}
