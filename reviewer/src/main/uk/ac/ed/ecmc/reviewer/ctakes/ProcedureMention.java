package uk.ac.ed.ecmc.reviewer.ctakes;

import org.w3c.dom.Node;

public class ProcedureMention extends AbstractCtakesMention{
	
	public ProcedureMention(Node node) {
		super(node);
	}

	@Override
	public String getType() {
		return "Procedure";
	}
}
