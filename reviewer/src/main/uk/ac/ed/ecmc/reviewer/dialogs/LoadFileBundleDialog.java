package uk.ac.ed.ecmc.reviewer.dialogs;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TerminalTextUtils;
import com.googlecode.lanterna.gui2.ActionListBox;
import com.googlecode.lanterna.gui2.Borders;
import com.googlecode.lanterna.gui2.Button;
import com.googlecode.lanterna.gui2.Direction;
import com.googlecode.lanterna.gui2.GridLayout;
import com.googlecode.lanterna.gui2.Label;
import com.googlecode.lanterna.gui2.Panel;
import com.googlecode.lanterna.gui2.Panels;
import com.googlecode.lanterna.gui2.Separator;
import com.googlecode.lanterna.gui2.TextBox;
import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.gui2.dialogs.DialogWindow;
import com.googlecode.lanterna.gui2.dialogs.MessageDialog;
import com.googlecode.lanterna.gui2.dialogs.MessageDialogButton;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoadFileBundleDialog extends DialogWindow {
    private static final Logger logger = LogManager.getLogger();
    private final ActionListBox fileListBox;
    private final ActionListBox directoryListBox;
    private final TextBox fileBox;
    private final Button okButton;

    private File directory;
    private File selectedFile = null;

    public LoadFileBundleDialog() {
        super("Load Bundle");
        this.selectedFile = null;

        File selectedObject = new File("").getAbsoluteFile();

        Panel contentPane = new Panel();
        contentPane.setLayoutManager(new GridLayout(2));

    
        new Label("Select bundle")
                .setLayoutData(
                        GridLayout.createLayoutData(
                                GridLayout.Alignment.BEGINNING,
                                GridLayout.Alignment.CENTER,
                                false,
                                false,
                                2,
                                1))
                .addTo(contentPane);
    
        TerminalSize dialogSize = new TerminalSize(45, 10);
        int unitWidth = dialogSize.getColumns() / 3;
        int unitHeight = dialogSize.getRows();

        new FileSystemLocationLabel()
                .setLayoutData(GridLayout.createLayoutData(
                        GridLayout.Alignment.FILL,
                        GridLayout.Alignment.CENTER,
                        true,
                        false,
                        2,
                        1))
                .addTo(contentPane);

        fileListBox = new ActionListBox(new TerminalSize(unitWidth * 2, unitHeight));
        fileListBox.withBorder(Borders.singleLine())
                .setLayoutData(GridLayout.createLayoutData(
                        GridLayout.Alignment.BEGINNING,
                        GridLayout.Alignment.CENTER,
                        false,
                        false))
                .addTo(contentPane);
        directoryListBox = new ActionListBox(new TerminalSize(unitWidth, unitHeight));
        directoryListBox.withBorder(Borders.singleLine())
                .addTo(contentPane);

        fileBox = new TextBox()
                .setLayoutData(GridLayout.createLayoutData(
                        GridLayout.Alignment.FILL,
                        GridLayout.Alignment.CENTER,
                        true,
                        false,
                        2,
                        1))
                .addTo(contentPane);

        new Separator(Direction.HORIZONTAL)
                .setLayoutData(
                        GridLayout.createLayoutData(
                                GridLayout.Alignment.FILL,
                                GridLayout.Alignment.CENTER,
                                true,
                                false,
                                2,
                                1))
                .addTo(contentPane);

        okButton = new Button("Open", new OkHandler());
        Panels.grid(2,
                okButton,
                new Button("Cancel", new CancelHandler()))
                .setLayoutData(GridLayout.createLayoutData(GridLayout.Alignment.END, GridLayout.Alignment.CENTER, false, false, 2, 1))
                .addTo(contentPane);

        if(selectedObject.isFile()) {
            directory = selectedObject.getParentFile();
            fileBox.setText(selectedObject.getName());
        }
        else if(selectedObject.isDirectory()) {
            directory = selectedObject;
        }

        reloadViews(directory);
        setComponent(contentPane);        
        setHints(Arrays.asList(Hint.CENTERED, Hint.MODAL));
    }

    @Override
    public File showDialog(WindowBasedTextGUI textGUI) {
        super.showDialog(textGUI);

        return selectedFile;
    }

    private File[] filterFileList(final File directory) {
        directoryListBox.clearItems();
        fileListBox.clearItems();

        List<File> result = new ArrayList<File>();

        try(DirectoryStream<Path> ds = Files.newDirectoryStream(directory.toPath())) {
            for(Path path : ds) {
                if (path.toFile().isDirectory()) {
                    result.add(path.toFile());
                } else {
                    String fileName = path.toString();
                    int lastIndexOf = fileName.lastIndexOf('.');
                    if ((lastIndexOf > 0) && (fileName.substring(lastIndexOf).equals(".rjson"))) {
                        result.add(path.toFile());
                    }
                }
            }
        }
        catch(IOException e) {
            logger.fatal("Issue streaming directory");
            logger.fatal(e.getMessage());
            System.exit(1);
        }

        return result.toArray(new File[result.size()]);
    }
    private void reloadViews(final File directory) {
        directoryListBox.clearItems();
        fileListBox.clearItems();

        File[] entries = filterFileList(directory);;
        if (entries == null) {
            return;
        }
        Arrays.sort(entries, Comparator.comparing(o -> o.getName().toLowerCase()));
        if (directory.getAbsoluteFile().getParentFile() != null) {
            directoryListBox.addItem("..", () -> {
                LoadFileBundleDialog.this.directory = directory.getAbsoluteFile().getParentFile();
                reloadViews(directory.getAbsoluteFile().getParentFile());
            });
        } else {
            File[] roots = File.listRoots();
            for (final File entry : roots) {
                if (entry.canRead()) {
                    directoryListBox.addItem('[' + entry.getPath() + ']', () -> {
                        LoadFileBundleDialog.this.directory = entry;
                        reloadViews(entry);
                    });
                }
            }
        }
        for (final File entry : entries) {
            if (entry.isHidden()) {
                continue;
            }
            if (entry.isDirectory()) {
                directoryListBox.addItem(entry.getName(), () -> {
                    LoadFileBundleDialog.this.directory = entry;
                    reloadViews(entry);
                });
            } else {
                fileListBox.addItem(entry.getName(), () -> {
                    fileBox.setText(entry.getName());
                    setFocusedInteractable(okButton);
                });
            }
        }
        if (fileListBox.isEmpty()) {
            fileListBox.addItem("<empty>", new DoNothing());
        }
    }

    private class OkHandler implements Runnable {
        @Override
        public void run() {
            if (!fileBox.getText().isEmpty()) {
                File file = new File(fileBox.getText());
                selectedFile = file.isAbsolute() ? file : new File(directory, fileBox.getText());
                close();
            } else {
                MessageDialog.showMessageDialog(getTextGUI(), "Error", "Please select a valid file name",
                        MessageDialogButton.OK);
            }
        }
    }

    private class CancelHandler implements Runnable {
        @Override
        public void run() {
            selectedFile = null;
            close();
        }
    }

    private static class DoNothing implements Runnable {
        @Override
        public void run() {
        }
    }

    private class FileSystemLocationLabel extends Label {
        public FileSystemLocationLabel() {
            super("");
            setPreferredSize(TerminalSize.ONE);
        }

        @Override
        public void onBeforeDrawing() {
            TerminalSize area = getSize();
            String absolutePath = directory.getAbsolutePath();
            int absolutePathLengthInColumns = TerminalTextUtils.getColumnWidth(absolutePath);
            if (area.getColumns() < absolutePathLengthInColumns) {
                absolutePath = absolutePath.substring(absolutePathLengthInColumns - area.getColumns());
                absolutePath = "..." + absolutePath.substring(Math.min(absolutePathLengthInColumns, 3));
            }
            setText(absolutePath);
        }
    }
}
