package uk.ac.ed.ecmc.reviewer.ctakes;

import org.w3c.dom.Node;

public class AnatomicalSiteMention extends AbstractCtakesMention  {
	public AnatomicalSiteMention(Node node) {
		super(node);
	}

	@Override
	public String getType() {
		return "Anatomical Site";
	}

}
