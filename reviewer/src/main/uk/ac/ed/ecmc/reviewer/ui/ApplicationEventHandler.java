package uk.ac.ed.ecmc.reviewer.ui;

import java.io.IOException;

import com.googlecode.lanterna.input.KeyStroke;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.ed.ecmc.reviewer.Application;

public class ApplicationEventHandler {
    private final Application application;
    private static Logger logger = LogManager.getLogger(ApplicationEventHandler.class);
    public ApplicationEventHandler(Application application) {
        this.application = application;
        try {
            messageLoop();
        }
        catch(IOException e) {
            logger.fatal("An error occured setting up the message loop");
            logger.fatal(e.getMessage());
        }
        
    }

    public void messageLoop() throws IOException {
        boolean exit = false;
		while(!exit) {
            application.getApplicationScreen().getTextGUI().getGUIThread().processEventsAndUpdate();			
			KeyStroke keyStroke = application.getApplicationScreen().pollInput();
			if (null != keyStroke) {
				switch (keyStroke.getKeyType()) {
				case Escape:
				case EOF:
					exit = true;
					break;
				case Character:
					processKeyStroke(keyStroke);
					break;
				case Tab:
					application.cycleActiveWindow();
                    break;
                case F2:
                    application.openFileBundle();
                    break;
                case F3:
                    application.switchAnnotations();
				default:
                    application.getApplicationScreen().handleInput(keyStroke);
				}
			}
		}
    }

    private void processKeyStroke(KeyStroke keyStroke) {
        application.getApplicationScreen().handleInput(keyStroke);
    }
}
