package uk.ac.ed.ecmc.reviewer.medcat;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.ed.ecmc.reviewer.annotations.Annotation;
import uk.ac.ed.ecmc.reviewer.annotations.MedCatAnnotation;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class MedCatAnnotationFileReader {
	private static final Logger logger = LogManager.getLogger();
	public static List<Annotation> readFromFile(File file) {
		try {
			FileReader fr = new FileReader(file);
			Gson gson = new Gson();
			Type listType = new TypeToken<List<MedCatAnnotation>>(){}.getType();
			return gson.fromJson(fr, listType);
		} catch (FileNotFoundException e) {
			logger.error("File not found!");
			return null;
		}
	}
	
	public static List<Annotation> readFromFile(String filename) {
		File file = new File(filename);
		return readFromFile(file);
	}	
}
