package uk.ac.ed.ecmc.reviewer.annotations;

import uk.ac.ed.ecmc.reviewer.dialogs.MoreInformationDialogBuilder;
import uk.ac.ed.ecmc.reviewer.ui.MainApplicationScreen;

public class MedCatRunnableAnnoation extends RunnableAnnotation {

    public MedCatRunnableAnnoation(MedCatAnnotation annotation) {
        super(annotation);
        
    }
    
    @Override
    public void run() {
        MedCatAnnotation mAnnotation = (MedCatAnnotation)annotation;
		MoreInformationDialogBuilder builder = new MoreInformationDialogBuilder()
			.setStart(mAnnotation.getStart())
			.setEnd(mAnnotation.getEnd())
			.setType(mAnnotation.getType())
            .setPrettyName(mAnnotation.getPrettyName())
            .setCui(mAnnotation.getCui())
            .setTui(mAnnotation.getTui())
            .setSourceValue(mAnnotation.getSourceValue())
            .setAcc(mAnnotation.getAcc());

        if ((mAnnotation.getInfo() != null)){
            builder.setSnomedCodes(mAnnotation.getInfo().getSnomed());
        }
		builder.build().showDialog(MainApplicationScreen.getInstance().getTextGUI());
    }

    @Override
    public String toString() {
        MedCatAnnotation a = (MedCatAnnotation)annotation;
        return String.format("%s [%d,%d] (%s)", a.getType(), a.getStart(), a.getEnd(), a.getPrettyName());
    }
}
