package uk.ac.ed.ecmc.reviewer.annotations;

public abstract class RunnableAnnotation implements Runnable {
	protected final Annotation annotation;
	
	public RunnableAnnotation(Annotation annotation) {
		this.annotation = annotation;
	}

	public Annotation getAnnotation() {
		return annotation;
	}

	public abstract String toString();
}
