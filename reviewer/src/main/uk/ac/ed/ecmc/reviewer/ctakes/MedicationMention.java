package uk.ac.ed.ecmc.reviewer.ctakes;

import org.w3c.dom.Node;

public class MedicationMention extends AbstractCtakesMention {
	private final Integer refMedicationStatusChange;
	private final Integer refMedicationForm;
	
	public MedicationMention(Node node) {
		super(node);
		
		NodeAttributes attributes = new NodeAttributes(node);

		refMedicationStatusChange = attributes.convertAttributeToInteger("_ref_medicationStatusChange");
		refMedicationForm = attributes.convertAttributeToInteger("_ref_medicationForm");
	}
	
	public Integer getRefMedicationStatusChange() {
		return refMedicationStatusChange;
	}

	public Integer getRefMedicationForm() {
		return refMedicationForm;
	}

	@Override
	public String getType() {
		return "Medication";
	}

}
