/**
 * 
 */
package uk.ac.ed.ecmc.reviewer;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * @author pmitche2
 *
 */
public class ArgumentParser {
	private CommandLine line;
	
	private ArgumentParser(String[] args ) {
		line = parseArguments(args);
	}
	
	private CommandLine parseArguments(String[] args) {
        Options options = getOptions();
        CommandLineParser parser = new DefaultParser();

        try {
            line = parser.parse(options, args);

        } catch (ParseException ex) {

            System.err.println("Failed to parse command line arguments");
            System.err.println(ex.toString());
            printAppHelp();

            System.exit(1);
        }

        if (hasHelp() || optionsEmpty()) {
        	printAppHelp();
        	
        	System.exit(0);
        }
        return line;
	}

	private boolean optionsEmpty() {
		return line.getOptions().length == 0;		
	}

	private void printAppHelp() {
        Options options = getOptions();

        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Reviewer", options, true);		
	}

	private Options getOptions() {
        Options options = new Options();

        options.addOption("f", "filename", true, "pathology report file name");
        options.addOption("c", "ctakes", true, "ctakes report file name");
        options.addOption("m", "medcat", true, "medcat report file name");
        options.addOption("?", "help", false, "Help");        
        
        return options;
	}

	public static ArgumentParser parse(String[] args) {
		return new ArgumentParser(args);
	}
	
	public Boolean hasPathologyReport() {
		return line.hasOption("filename");
	}
	
	public Boolean hasCtakesData() {
		return line.hasOption("ctakes");
	}
	
	public Boolean hasMedCatData() {
		return line.hasOption("medcat");
	}
	
	public Boolean hasHelp() {
		return line.hasOption("?");
	}
	
	public String pathologyReport() {
		return line.getOptionValue("filename");
	}
	
	public String ctakesData() {
		return line.getOptionValue("ctakes");
	}
	
	public String medcatData() {
		return line.getOptionValue("medcat");
	}
}
