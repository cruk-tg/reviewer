/**
 * 
 */
package uk.ac.ed.ecmc.reviewer.ui;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;

/**
 * @author pmitche2
 *
 */
public class ApplicationTerminal {
	protected static final Logger logger = LogManager.getLogger();
	private DefaultTerminalFactory defaultTerminalFactory = new DefaultTerminalFactory();
	private Terminal terminal = null;
	private TextGraphics textGraphics;
	
	public ApplicationTerminal() {
	    initializeTerminal();
	    initializeTextGraphics();
	}
	
	private void initializeTerminal() {
        try {
			terminal = defaultTerminalFactory.createTerminal();
			terminal.enterPrivateMode();
	        terminal.clearScreen();
	        terminal.setCursorVisible(false);
		} catch (IOException e) {
			logger.fatal("An error occured attempting to setup the terminal window");
			logger.fatal(e);
			System.exit(1);
		}		
	}
	
	private void initializeTextGraphics() {
        try {
			textGraphics = terminal.newTextGraphics();
	        textGraphics.setForegroundColor(TextColor.ANSI.WHITE);
	        textGraphics.setBackgroundColor(TextColor.ANSI.BLACK);
		} catch (IOException e) {
			logger.fatal("An error occured attempting to create a new text graphics buffer");
			logger.fatal(e);
			System.exit(1);
		}

	}
	
	public Terminal getTerminal() {
		return terminal;
	}
	
	public TextGraphics getTextGraphics() {
		return textGraphics;
	}

	public TerminalSize getTerminalSize() {
		try {
			return terminal.getTerminalSize();
		} catch (IOException e) {
			logger.fatal("Failed to get terminal size");
			logger.fatal(e);
			System.exit(1);
		}
		
		return null;
	}

	public void flush() {
		try {
			terminal.flush();
		} catch (IOException e) {
			logger.error("Error flushing terminal buffer");
			logger.error(e);
		}		
	}

	public KeyStroke readInput() {
		try {
			return terminal.readInput();
		} catch (IOException e) {
			logger.fatal("Could not open read input");
			logger.fatal(e);
			System.exit(1);
		}
		
		return null;
	}
}
