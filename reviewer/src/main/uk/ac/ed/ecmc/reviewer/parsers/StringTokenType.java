package uk.ac.ed.ecmc.reviewer.parsers;

public enum StringTokenType {
	NEWLINE,
	PUNCUTATION,
	SPACE,
	STRING,
	UNKNOWN
}
