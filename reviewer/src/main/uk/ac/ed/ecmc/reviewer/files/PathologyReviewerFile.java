package uk.ac.ed.ecmc.reviewer.files;

import java.io.File;
import java.util.List;

import uk.ac.ed.ecmc.reviewer.parsers.StringToken;
import uk.ac.ed.ecmc.reviewer.readers.PathologyReportReader;

public class PathologyReviewerFile implements IReviewerFile {    
    private PathologyReportReader reader;

    public PathologyReviewerFile(File file) {
        reader = new PathologyReportReader(file);
        reader.read();
    }

    public List<StringToken> getTokens() {
        return reader.getStringTokens();
    }
}
