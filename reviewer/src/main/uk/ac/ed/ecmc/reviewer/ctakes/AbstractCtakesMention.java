package uk.ac.ed.ecmc.reviewer.ctakes;

import java.util.Collections;
import java.util.List;

import org.w3c.dom.Node;

import uk.ac.ed.ecmc.reviewer.annotations.AdditionalItem;
import uk.ac.ed.ecmc.reviewer.annotations.Annotation;

public abstract class AbstractCtakesMention implements Annotation, IIdable {

	protected final Integer indexed;
	protected final Integer underscoreId;
	protected final Integer refSofa;
	protected Integer begin;
	protected Integer end;
	protected final Integer id;
	protected final Integer refOntologyConceptArrId;
	protected final Integer typeId;
	protected final String segmentId;
	protected final Integer discoveryTechnique;
	protected final Double confidence;
	protected final Integer polarity;
	protected final Integer uncertainty;
	protected final Boolean conditional;
	protected final Boolean generic;
	protected final String subject;
	protected final Integer historyOf;
	protected UmlsConcept umlsConcept;

	public AbstractCtakesMention(Node node) {
		NodeAttributes attributes = new NodeAttributes(node);

		indexed = attributes.convertAttributeToInteger("_indexed");
		underscoreId = attributes.convertAttributeToInteger("_id");
		refSofa = attributes.convertAttributeToInteger("_ref_sofa");
		begin = attributes.convertAttributeToInteger("begin");
		end = attributes.convertAttributeToInteger("end");
		id = attributes.convertAttributeToInteger("id");
		refOntologyConceptArrId = attributes.convertAttributeToInteger("_ref_ontologyConceptArr");
		typeId = attributes.convertAttributeToInteger("typeID");
		segmentId = attributes.convertAttributeToString("segmentID");
		discoveryTechnique = attributes.convertAttributeToInteger("discoveryTechnique");
		confidence = attributes.convertAttributeToDouble("confidence");
		polarity = attributes.convertAttributeToInteger("polarity");
		uncertainty = attributes.convertAttributeToInteger("uncertainty");
		conditional = attributes.convertAttributeToBoolean("conditional");
		generic = attributes.convertAttributeToBoolean("generic");
		subject = attributes.convertAttributeToString("subject");
		historyOf = attributes.convertAttributeToInteger("historyOf");
	}

	public Integer getIndexed() {
		return indexed;
	}

	public Integer getUnderscoreId() {
		return underscoreId;
	}

	public Integer getRefSofa() {
		return refSofa;
	}

	public Integer getBegin() {
		return begin;
	}

	public Integer getEnd() {
		return end;
	}

	public Integer getId() {
		return id;
	}

	public Integer getRefOntologyConceptArrId() {
		return refOntologyConceptArrId;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public String getSegmentId() {
		return segmentId;
	}

	public Integer getDiscoveryTechnique() {
		return discoveryTechnique;
	}

	public Double getConfidence() {
		return confidence;
	}

	public Integer getPolarity() {
		return polarity;
	}

	public Integer getUncertainty() {
		return uncertainty;
	}

	public Boolean getConditional() {
		return conditional;
	}

	public Boolean getGeneric() {
		return generic;
	}

	public String getSubject() {
		return subject;
	}

	public Integer getHistoryOf() {
		return historyOf;
	}

	public UmlsConcept getUmlsConcept() {
		return umlsConcept;
	}

	public void setUmlsConcept(UmlsConcept v) {
		umlsConcept = v;
	}

	public void adjustsForNewline() {
		begin = begin - 1;
		end = end - 1;
	}

	@Override
	public Integer getStart() {
		return begin;
	}

	@Override
	public String toString() {
		return String.format("%s (%d, %d)", getType(), getStart(), getEnd());
	}

	@Override
	public int compareTo(IIdable other) {
		if (other instanceof Annotation) {
			Annotation o = (Annotation)other;
			return o.getStart().compareTo(o.getStart());
		} else {
			if (this.getType().compareTo(other.getType()) == 0) {
				return this.getId().compareTo(other.getId());
			}
			return this.getType().compareTo(other.getType());
		}
	}

	@Override
	public List<AdditionalItem> getAdditionalInformation() {	
		return Collections.emptyList();
	}
}