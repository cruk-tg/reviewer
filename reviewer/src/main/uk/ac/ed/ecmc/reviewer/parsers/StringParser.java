package uk.ac.ed.ecmc.reviewer.parsers;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class StringParser {
	private final String string;
	private final ArrayList<StringToken> tokens = new ArrayList<StringToken>();
	private static final Pattern letters = Pattern.compile("\\p{Alpha}");
	private static final Pattern numbers = Pattern.compile("\\p{Digit}");
	private static final Pattern puncutation = Pattern.compile("\\p{Punct}");
	private static final Pattern space = Pattern.compile("\\p{Space}");
	
	public static List<StringToken> parse(String string) {
		return new StringParser(string).execute();
	}
	
	private StringParser(String string) {
		this.string = string;
	}
	
	private List<StringToken> execute() {
		CharacterIterator iter = new StringCharacterIterator(string);
		int count = 0;
		int start = 0;
		String buffer = "";
		StringTokenType currentType = StringTokenType.UNKNOWN;
		for(char c = iter.first(); c != CharacterIterator.DONE; c = iter.next()) {
			count++;
			
			if (StringTokenType.UNKNOWN == currentType) {
				currentType = getStringTokenType(c);
			}
			if (currentType != getStringTokenType(c)) {
				tokens.add(new StringToken(currentType, buffer, start, count));
				currentType = getStringTokenType(c);
				start = count;
				buffer = String.valueOf(c);
			} else {
				buffer = buffer.concat(String.valueOf(c));	
			}
			
		}
		tokens.add(new StringToken(currentType, String.valueOf(buffer), start, count));
		return tokens;
	}

	private StringTokenType getStringTokenType(char c) {
		
		if (letters.matcher(String.valueOf(c)).find()) {
			return StringTokenType.STRING;	
		} else if (numbers.matcher(String.valueOf(c)).find()) {
			return StringTokenType.STRING;
		} else if (puncutation.matcher(String.valueOf(c)).find()) {
			return StringTokenType.PUNCUTATION;
		} else if (space.matcher(String.valueOf(c)).find()) {
			if (c == 0x0a) {
				return StringTokenType.NEWLINE;
			}
			return StringTokenType.SPACE;
		} else {
			return StringTokenType.UNKNOWN;
		}
		
	}
}
