/**
 * 
 */
package uk.ac.ed.ecmc.reviewer.ui;

import java.io.File;

import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.gui2.dialogs.FileDialog;
import com.googlecode.lanterna.gui2.dialogs.FileDialogBuilder;

/**
 * @author pmitche2
 *
 */
public class PathologyReportFileDialog {
	private final FileDialog fd;
	private final WindowBasedTextGUI textGUI;
	public PathologyReportFileDialog(WindowBasedTextGUI  textGUI) {
		this.textGUI = textGUI;
		fd = new FileDialogBuilder()
				.setTitle("Open Pathology Report")
				.setDescription("Choose a file")
				.setActionLabel("Open")
				.build();
	}
	
	public File open() {
		return fd.showDialog(textGUI);
	}
}
