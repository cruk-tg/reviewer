/**
 * 
 */
package uk.ac.ed.ecmc.reviewer.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.AbsoluteLayout;
import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.Component;
import com.googlecode.lanterna.gui2.Direction;
import com.googlecode.lanterna.gui2.Panel;
import com.googlecode.lanterna.gui2.ScrollBar;
import com.googlecode.lanterna.input.KeyStroke;

import uk.ac.ed.ecmc.reviewer.parsers.StringToken;
import uk.ac.ed.ecmc.reviewer.ui.components.AbstractReportBlockComponent;
import uk.ac.ed.ecmc.reviewer.ui.components.NewlineComponent;
import uk.ac.ed.ecmc.reviewer.ui.components.PathologyReportWord;
import uk.ac.ed.ecmc.reviewer.ui.components.SpaceComponent;

/**
 * @author pmitche2
 *
 */
public class PathologyReportWindow extends BasicWindow {
	private final AbsoluteLayout defaultLayout = new AbsoluteLayout(); 

	private Panel defaultPanel = new Panel(defaultLayout);
	private ArrayList<GuiWord> wordItems = null;
	private ScrollBar scrollBar;
	private int scrollPos = 0;
	private int documentRows = 0;
	
	private class GuiWord {
		private final AbstractReportBlockComponent<?> component;
		private final StringToken token;
		
		public GuiWord(StringToken token) {
			this.token = token;
			switch(token.getType()) {
			case NEWLINE:
				this.component = new NewlineComponent();
				break;
			case STRING:
				this.component = new PathologyReportWord(token.getString());
				break;	
			case SPACE:
				this.component = new SpaceComponent();
				break;
			case UNKNOWN:
				this.component = new PathologyReportWord(token.getString());
				break;
			case PUNCUTATION:
				this.component = new PathologyReportWord(token.getString());
				break;
			default:
				this.component = new PathologyReportWord(token.getString());
				break;
			}
		}

		public AbstractReportBlockComponent<?>  getComponent() {
			return component;
		}
		
		public StringToken getToken() {
			return token;
		}

	}
	private class WordLayout {
		private final ArrayList<GuiWord> collection;
		private TerminalPosition currentPosition = TerminalPosition.TOP_LEFT_CORNER;
		private TerminalSize panelSize = null;
		public WordLayout(ArrayList<GuiWord> collection) {
			super();
			this.collection = collection;
		}

		public int layoutPanel(Panel panel) {
			panelSize = panel.getSize();
			removeAllWordComponents(panel);
			for(GuiWord guiWord: collection) {
				Component component = guiWord.getComponent();
				if (component instanceof NewlineComponent) {
					nextLine();
					continue;
				}
				if (willBreak(guiWord.getToken().getString())) {
					nextLine();
				}
				component.setPosition(currentPosition);
				panel.addComponent(component);
				movePositionByComponent(component);					
			}
			
			return currentPosition.getRow();
		}
		
		private void removeAllWordComponents(Panel panel) {
			for(Component component: panel.getChildrenList()) {
				if (!(component instanceof ScrollBar)) {
					panel.removeComponent(component);	
				}				
			}
		}
		private boolean willBreak(String word) {
			return (currentPosition.getColumn() + word.length()) > panelSize.getColumns();
		}
		
		private void movePositionByComponent(Component component) {
			currentPosition = currentPosition.withRelativeColumn(component.getSize().getColumns());			
		}
		
		private void nextLine() {
			currentPosition = currentPosition.withRelativeRow(1).withColumn(0);
		}
		
		
	}
	public PathologyReportWindow() {
		super("Pathology Text");						
		this.setComponent(defaultPanel);
		this.setHints(Collections.singletonList(Hint.NO_POST_RENDERING));		
	}


	public void setReport(List<StringToken> tokens) {				
		defaultPanel.removeAllComponents();
		wordItems = new ArrayList<PathologyReportWindow.GuiWord>();
		for(StringToken token : tokens) {
			wordItems.add(new GuiWord(token));
		}
		documentRows = new WordLayout(wordItems).layoutPanel(defaultPanel);
		createScrollbar();
	}

	
	public void highlightWord(int start, int end) {
		for (GuiWord guiWord: wordItems) {
			AbstractReportBlockComponent<?> component = guiWord.getComponent();
			if ((guiWord.getToken().getStart() > start || (start == 0)) && guiWord.getToken().getEnd() <= end) {				
				component.setHighlighted(true);			
				while (!isComponentVisible(component)) {
					int cRow = component.getPosition().getRow();
					if (cRow < 0) {
						scroll(ScrollDirection.UP);
					} else {
						scroll(ScrollDirection.DOWN);
					}
				}
			} else {
				component.setHighlighted(false);
			}
		}
	}

	private boolean isComponentVisible(Component component) {
		TerminalPosition pos = component.getPosition();
		TerminalSize pSize = defaultPanel.getSize();
		if ((pos.getRow() < 0) || (pos.getRow() > pSize.getRows() - 1)) {
			return false;
		} 
		return true;
	}
	
	@Override
	public boolean handleInput(KeyStroke key) {
		switch (key.getKeyType()) {
		case ArrowDown:
			scroll(ScrollDirection.DOWN);			
			break;
		case ArrowUp:
			scroll(ScrollDirection.UP);			
			break;
		default:
			return super.handleInput(key);
		}
		
		return true;
	}


	private void scroll(ScrollDirection direction) {
		if (direction.equals(ScrollDirection.DOWN) && (!scrollAtEndOfDocument())) {
			scrollPos++;			
		} else if (direction.equals(ScrollDirection.UP) && (scrollPos > 0)){
			scrollPos--;
		} else {
			return;
		}
		for(Component component: this.defaultPanel.getChildrenList()) {
			if (component instanceof ScrollBar) {
				continue;
			}
			if (direction == ScrollDirection.UP) {
				component.setPosition(component.getPosition().withRelativeRow(1));
			} else {
				component.setPosition(component.getPosition().withRelativeRow(-1));
			}
			
			component.invalidate();
		}
		scrollBar.setScrollPosition(scrollbarPosition());		
	}
	
	private boolean scrollAtEndOfDocument() {
		return (scrollPos + defaultPanel.getSize().getRows()) >= documentRows;
	}
	
	private void createScrollbar() {
		scrollBar = new ScrollBar(Direction.VERTICAL);
		TerminalSize size = this.getSize();
		scrollBar.setPosition(new TerminalPosition(size.getColumns() - 1, 0));
		scrollBar.setSize(new TerminalSize(1, size.getRows()));
		scrollBar.setScrollMaximum(documentRows);
		scrollBar.setScrollPosition(0);
		scrollBar.setViewSize(size.getRows());
		defaultPanel.addComponent(scrollBar);
	}
	
	private int scrollbarPosition() {
		if (scrollAtEndOfDocument()) {
			return documentRows;
		} else {
			return scrollPos;
		}
	}
}
