package uk.ac.ed.ecmc.reviewer.dialogs;

import com.googlecode.lanterna.gui2.Label;

public class MoreInformationDialogBuilder {
    private String prettyName = "";
    private String cui = "";
    private String tui = "";
    private String type = "";
    private String sourceValue = "";
    private Double acc = 0.0;
    private int start = 0;
    private int end = 0;
    private String[] snomedCodes = new String[] {};

    public MoreInformationDialogBuilder() {

    }

    public MoreInformationDialogBuilder setPrettyName(String v) {
        prettyName = v;
        return this;
    }

    public MoreInformationDialogBuilder setCui(String v) {
        cui = v;
        return this;
    }

    public MoreInformationDialogBuilder setTui(String v) {
        tui = v;
        return this;
    }

    public MoreInformationDialogBuilder setType(String v) {
        type = v;
        return this;
    }

    public MoreInformationDialogBuilder setSourceValue(String v) {
        sourceValue = v;
        return this;
    }

    public MoreInformationDialogBuilder setAcc(Double v) {
        acc = v;
        return this;
    }

    public MoreInformationDialogBuilder setStart(int v) {
        start = v;
        return this;
    }

    public MoreInformationDialogBuilder setEnd(int v) {
        end = v;
        return this;
    }

    public MoreInformationDialogBuilder setSnomedCodes(String[] codes) {
        snomedCodes = codes;
        return this;
    }

    public MoreInformationDialog build() {
        MoreInformationDialog dialog = new MoreInformationDialog("More Info");

        new Label("Pretty Name :").addTo(dialog.getLabelsPanel());
        new Label(prettyName).addTo(dialog.getValuesPanel());
        new Label("CUI         :").addTo(dialog.getLabelsPanel());
        new Label(cui).addTo(dialog.getValuesPanel());
        new Label("TUI         :").addTo(dialog.getLabelsPanel());
        new Label(tui).addTo(dialog.getValuesPanel());
        new Label("Type        :").addTo(dialog.getLabelsPanel());
        new Label(type).addTo(dialog.getValuesPanel());
        new Label("Source Value:").addTo(dialog.getLabelsPanel());
        new Label(sourceValue).addTo(dialog.getValuesPanel());
        new Label("ACC         :").addTo(dialog.getLabelsPanel());
        new Label(String.format("%f04", acc)).addTo(dialog.getValuesPanel());
        new Label("Start       :").addTo(dialog.getLabelsPanel());
        new Label(String.format("%d", start)).addTo(dialog.getValuesPanel());
        new Label("End         :").addTo(dialog.getLabelsPanel());
        new Label(String.format("%d", end)).addTo(dialog.getValuesPanel());
        new Label("Snomed      :").addTo(dialog.getLabelsPanel());
        for (String string : snomedCodes) {
            new Label(string).addTo(dialog.getValuesPanel());
        }
        return dialog;
    }
}
