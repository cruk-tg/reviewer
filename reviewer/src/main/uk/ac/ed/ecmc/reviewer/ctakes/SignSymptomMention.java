package uk.ac.ed.ecmc.reviewer.ctakes;

import org.w3c.dom.Node;

public class SignSymptomMention extends AbstractCtakesMention {
	public SignSymptomMention(Node node) {
		super(node);
	}

	@Override
	public String getType() {
		return "Sign/Symptom";
	}
}
