package uk.ac.ed.ecmc.reviewer.ui;

import uk.ac.ed.ecmc.reviewer.annotations.Annotation;

public interface SelectionChangeListener {
	void onSelectionChange(Annotation annotation);
}
