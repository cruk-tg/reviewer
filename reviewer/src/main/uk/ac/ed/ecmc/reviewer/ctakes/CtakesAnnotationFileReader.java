package uk.ac.ed.ecmc.reviewer.ctakes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import uk.ac.ed.ecmc.reviewer.annotations.Annotation;

public class CtakesAnnotationFileReader {	
	private static final Logger logger = LogManager.getLogger();
	private final Document document;
	private CtakesSet items = new CtakesSet();

	private CtakesAnnotationFileReader(Document document) {
		this.document = document;
		populateAnnotations();
		populateFsArray();
		adjustForNewlines();
		addUmlsRefsToAnnotations();
	}
	
	public CtakesSet getCtakesSet() {
		return items;
	}
	
	public class EmptyNodeList implements NodeList {

		@Override
		public Node item(int index) {
			return null;
		}

		@Override
		public int getLength() {
			return 0;
		}
		
	}
	
	private void populateAnnotations() {
		NodeList tokens = getAnatomicalSiteMention();
		for(int i=0;i<tokens.getLength();i++) {
			AnatomicalSiteMention annotation = new AnatomicalSiteMention(tokens.item(i));
			items.add(annotation);			
		}
		tokens = getMedicationMention();
		for(int i=0;i<tokens.getLength();i++) {
			MedicationMention annotation = new MedicationMention(tokens.item(i));
			
			items.add(annotation);
		}
		tokens = getProcedureMention();
		for(int i=0;i<tokens.getLength();i++) {
			ProcedureMention annotation = new ProcedureMention(tokens.item(i));
			
			items.add(annotation);
		}
		tokens = getDiseaseDisorderMention();
		for(int i=0;i<tokens.getLength();i++) {
			DiseaseDisorderMention annotation = new DiseaseDisorderMention(tokens.item(i));
			
			items.add(annotation);
		}
		tokens = getSignSymptomMention();
		for(int i=0;i<tokens.getLength();i++) {
			SignSymptomMention annotation = new SignSymptomMention(tokens.item(i));
			
			items.add(annotation);
		}
	}

	private void populateFsArray() {
		NodeList arrays = getFSArrays();
		for(int i=0;i<arrays.getLength();i++) {
			items.add(new FSArray(arrays.item(i)));
		}
	}

	private void adjustForNewlines() {
		ArrayList<NewlineToken> newlines = new ArrayList<NewlineToken>();
		NodeList nodes = getNewLines();
		for(int i = 0; i < nodes.getLength(); i++) {
			newlines.add(new NewlineToken(nodes.item(i)));
		}
		newlines.sort(new Comparator<NewlineToken>() {

			@Override
			public int compare(NewlineToken o1, NewlineToken o2) {				
				return o1.getBegin().compareTo(o2.getBegin());
			}
		});
		for(Annotation annotation : items.annotations())			
			for (NewlineToken newlineToken : newlines) {
				if (annotation.getStart() > newlineToken.getBegin()) {
					if (annotation instanceof AbstractCtakesMention) {
						((AbstractCtakesMention)annotation).adjustsForNewline();
					}
				} else {
					break;
				}
			}
		}
	
	private NodeList getSignSymptomMention() {
		return getXPathNodeListFrom("/CAS/org.apache.ctakes.typesystem.type.textsem.SignSymptonMention");
	}

	private NodeList getDiseaseDisorderMention() {
		return getXPathNodeListFrom("/CAS/org.apache.ctakes.typesystem.type.textsem.DiseaseDisorderMention");
	}

	private NodeList getFSArrays( ) {
		return getXPathNodeListFrom("/CAS/uima.cas.FSArray");
	}
	
	private NodeList getMedicationMention( ) {
		return getXPathNodeListFrom("/CAS/org.apache.ctakes.typesystem.type.textsem.MedicationMention");
	}
	
	private NodeList getProcedureMention( ) {
		return getXPathNodeListFrom("/CAS/org.apache.ctakes.typesystem.type.textsem.ProcedureMention");
	}
	
	private NodeList getAnatomicalSiteMention( ) {
		return getXPathNodeListFrom("/CAS/org.apache.ctakes.typesystem.type.textsem.AnatomicalSiteMention");
	}
	
	private NodeList getNewLines( ) {
		return getXPathNodeListFrom("/CAS/org.apache.ctakes.typesystem.type.syntax.NewlineToken");
	}

	private NodeList getXPathNodeListFrom(String expression) {
		try {
			return (NodeList) XPathFactory
					.newInstance()
					.newXPath()
					.compile(expression)
					.evaluate(document, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			logger.error("Invalid XPath Expression: {}", expression);
			return new EmptyNodeList();
		}
	}

	private void addUmlsRefsToAnnotations() {
		for(Annotation annotation : items.annotations()) {
			AbstractCtakesMention aAnnoation = (AbstractCtakesMention)annotation;
			IIdable iIdable = items.find(aAnnoation.refOntologyConceptArrId);
			if (iIdable instanceof FSArray) {
				FSArray array = (FSArray)iIdable;
				Iterator<IIdable> iter = array.getContainer().values().iterator();
				if (iter.hasNext()) {
					aAnnoation.umlsConcept = (UmlsConcept)iter.next();
				}				
			}
		}
	}

	public static CtakesSet readFromFile(File file) {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try(InputStream stream = new FileInputStream(file)) {
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(stream);
			
			return new CtakesAnnotationFileReader(doc).getCtakesSet();
			
		} catch (FileNotFoundException e) {
			logger.error("File not found: {}", file.getAbsoluteFile());
			return null;
		} catch (IOException e) {
			logger.error("An IO error occured in reading the CTakes Annotation File");
			logger.error(e);
			return null;
		} catch (ParserConfigurationException e) {
			logger.error("An error occured parser the XML document");
			logger.error(e);
			return null;
		} catch (SAXException e) {
			logger.error("An error occured parser the XML document");
			logger.error(e);
			return null;
		}
	}
}
