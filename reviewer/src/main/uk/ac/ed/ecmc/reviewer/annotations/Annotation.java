package uk.ac.ed.ecmc.reviewer.annotations;

import java.util.List;

public interface Annotation {
	public Integer getId();
	public Integer getStart();
	public Integer getEnd();
	public String getType();
	public List<AdditionalItem> getAdditionalInformation();	
}
