package uk.ac.ed.ecmc.reviewer.ui;

import java.util.List;

import com.googlecode.lanterna.gui2.ActionListBox;
import com.googlecode.lanterna.gui2.BasicWindow;
import com.googlecode.lanterna.gui2.Interactable.Result;
import com.googlecode.lanterna.input.KeyStroke;

import uk.ac.ed.ecmc.reviewer.annotations.Annotation;
import uk.ac.ed.ecmc.reviewer.annotations.CtakesRunnableAnnotation;
import uk.ac.ed.ecmc.reviewer.annotations.MedCatAnnotation;
import uk.ac.ed.ecmc.reviewer.annotations.MedCatRunnableAnnoation;
import uk.ac.ed.ecmc.reviewer.annotations.RunnableAnnotation;
import uk.ac.ed.ecmc.reviewer.ctakes.AbstractCtakesMention;
import uk.ac.ed.ecmc.reviewer.ctakes.CtakesSet;
import uk.ac.ed.ecmc.reviewer.files.AbstractReviewerFile;
import uk.ac.ed.ecmc.reviewer.files.CtakesReviewerFile;
import uk.ac.ed.ecmc.reviewer.files.MedCatReviewerFile;

public class NLPDataWindow extends BasicWindow {
	private ActionListBox  listbox = new ActionListBox ();
	private SelectionChangeListener selectionChangeListener;

	public NLPDataWindow() {
		super("NLP Data");
		//this.setTheme(LanternaThemes.getDefaultTheme());
		this.setComponent(listbox);

	}

	public void setDocument(AbstractReviewerFile arf) {
		while(listbox.getItemCount() > 0) {
			listbox.removeItem(0);
		}
		
		if (arf instanceof MedCatReviewerFile) {
			medCatAnnotations(arf.getAnnotations());
		} else {
			CtakesReviewerFile r = (CtakesReviewerFile)arf;
			ctakesAnnotations(arf.getAnnotations(), r.getCtakesSet());
		}
		setSelectionChangeMessage((RunnableAnnotation)listbox.getItemAt(0));
	}



	public void setSelectionChangeListener(SelectionChangeListener selectionChangeListener) {
		this.selectionChangeListener = selectionChangeListener;
	}
	
	@Override
	public boolean handleInput(KeyStroke key) {
		if (listbox.handleInput(key) == Result.HANDLED) {
			RunnableAnnotation ra = (RunnableAnnotation)listbox.getSelectedItem();
			setSelectionChangeMessage(ra);
			return true;
		}

		return super.handleInput(key);
	}

	private void setSelectionChangeMessage(RunnableAnnotation ra) {
		selectionChangeListener.onSelectionChange(ra.getAnnotation());
	}

	private void medCatAnnotations(List<Annotation> annotations) {
		for(Annotation a: annotations) {
			listbox.addItem(new MedCatRunnableAnnoation((MedCatAnnotation) a));
		}
	}

	private void ctakesAnnotations(List<Annotation> annotations, CtakesSet set) {
		for(Annotation a: annotations) {
			listbox.addItem(new CtakesRunnableAnnotation((AbstractCtakesMention) a, set));
		}
	}
}
