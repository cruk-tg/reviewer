package uk.ac.ed.ecmc.reviewer.ui.components;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.ComponentRenderer;
import com.googlecode.lanterna.gui2.TextGUIGraphics;

public class NewlineComponent extends AbstractReportBlockComponent<NewlineComponent> {

	public NewlineComponent() {
		this.setSize(new TerminalSize(0, 0));
	}
	
	@Override
	protected ComponentRenderer<NewlineComponent> createDefaultRenderer() {
		return new ComponentRenderer<NewlineComponent>() {

			@Override
			public TerminalSize getPreferredSize(NewlineComponent component) {
				return new TerminalSize(0, 0);
			}

			@Override
			public void drawComponent(TextGUIGraphics graphics, NewlineComponent component) {
				
			}
		
		};
	}
}
