package uk.ac.ed.ecmc.reviewer.ui;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.DefaultWindowManager;
import com.googlecode.lanterna.gui2.Window;
import com.googlecode.lanterna.gui2.WindowDecorationRenderer;

public class ApplicationWindowManager extends DefaultWindowManager {    
    private TerminalSize lastKnownScreenSize;

    public ApplicationWindowManager() {
        this(null);
    }

    public ApplicationWindowManager(TerminalSize initialScreenSize) {
        this(null, initialScreenSize);
    }

    public ApplicationWindowManager(WindowDecorationRenderer windowDecorationRenderer, TerminalSize initialScreenSize) {        
        if(initialScreenSize != null) {
            this.lastKnownScreenSize = initialScreenSize;
        }
        else {
            this.lastKnownScreenSize = new TerminalSize(80, 24);
        }
    }
	
    @Override
	public void prepareWindow(TerminalSize size, Window window) {
    	if (window instanceof PathologyReportWindow) {
    		preparePathologyWindow(window);	
    	} else if (window instanceof NLPDataWindow) {
    		prepareNLPDataWindow(window);
    	} else {
    		super.prepareWindow(size, window);
    	}
	}

	private void preparePathologyWindow(Window window) {
		TerminalPosition position = TerminalPosition.TOP_LEFT_CORNER;
		TerminalSize size = new TerminalSize(this.lastKnownScreenSize.getColumns(), pathologyWindowRows());
        window.setPosition(position);
        window.setDecoratedSize(size);		
	}
	
	private int pathologyWindowRows() {
		return Math.floorDiv(lastKnownScreenSize.getRows() * 2, 3);
	}
	
	private void prepareNLPDataWindow(Window window) {
		int top = pathologyWindowRows();
		TerminalPosition position = new TerminalPosition(0, top);
		int height = lastKnownScreenSize.getRows() - pathologyWindowRows();
		TerminalSize size = new TerminalSize(lastKnownScreenSize.getColumns(), height);
		
		window.setPosition(position);
		window.setDecoratedSize(size);
	}
}
