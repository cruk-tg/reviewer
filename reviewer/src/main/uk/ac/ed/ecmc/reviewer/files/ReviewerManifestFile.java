package uk.ac.ed.ecmc.reviewer.files;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.Path;

import com.google.gson.Gson;

public class ReviewerManifestFile {
    private final Path onDiskLocation;
    private CtakesReviewerFile ctakesReviewerFile;
    private MedCatReviewerFile medCatReviewerFile;
    private PathologyReviewerFile pathologyReviewerFile;

    public CtakesReviewerFile getCtakesReviewerFile() {
        return ctakesReviewerFile;
    }

    public MedCatReviewerFile getMedCatReviewerFile() {
        return medCatReviewerFile;
    }

    public PathologyReviewerFile getPathologyReviewerFile() {
        return pathologyReviewerFile;
    }

    public static ReviewerManifestFile readFile(File file) throws FileNotFoundException {
        FileReader fr = new FileReader(file);
        Gson gson = new Gson();
        ReviewerManifestFileFormat inst = gson.fromJson(fr, ReviewerManifestFileFormat.class);        

        return new ReviewerManifestFile(inst, file.toPath());
    }

    private ReviewerManifestFile(ReviewerManifestFileFormat onDisk, Path toFile) throws FileNotFoundException {
        this.onDiskLocation = toFile.getParent();

        File file = findFile(onDisk.pathologyReviewerFile);
        if (!file.exists()) {
            throw new FileNotFoundException(onDisk.pathologyReviewerFile);
        }
        pathologyReviewerFile = new PathologyReviewerFile(file);
        file = findFile(onDisk.medCatReviewerFile);
        if (file.exists()) {
            medCatReviewerFile = new MedCatReviewerFile(file);
        }
        file = findFile(onDisk.ctakesReviewerFile);
        if (file.exists()) {
            ctakesReviewerFile = new CtakesReviewerFile(file);
        }
    }

    private File findFile(String pathname) {
        File file = new File(pathname);
        if(!file.exists()) {
            // Maybe rel to this file
            Path joined = onDiskLocation.resolve(file.toPath());
            file = joined.toFile();
        }
        return file;
    }

    private class ReviewerManifestFileFormat {
        public String ctakesReviewerFile;
        public String medCatReviewerFile;
        public String pathologyReviewerFile;
    }
}
