package uk.ac.ed.ecmc.reviewer.files;

import java.io.File;
import java.util.List;

import uk.ac.ed.ecmc.reviewer.annotations.Annotation;
import uk.ac.ed.ecmc.reviewer.ctakes.CtakesAnnotationFileReader;
import uk.ac.ed.ecmc.reviewer.ctakes.CtakesSet;

public class CtakesReviewerFile extends AbstractReviewerFile {
    private final File underlyingFile;
    private final CtakesSet set;

    public CtakesReviewerFile(File file) {
        underlyingFile = file;
        set = CtakesAnnotationFileReader.readFromFile(underlyingFile);
    }

    @Override
    public List<Annotation> getAnnotations() {
        return set.annotations();
    }

    public CtakesSet getCtakesSet() {
        return set;
    }
}
