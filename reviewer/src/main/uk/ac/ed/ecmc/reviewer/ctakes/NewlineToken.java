package uk.ac.ed.ecmc.reviewer.ctakes;

import org.w3c.dom.Node;

public class NewlineToken implements IIdable {
  private final Integer id;
  private final Integer indexed;
  private final Integer refSofa;
  private final Integer begin;
  private final Integer end;
  private final Integer tokenNumber;

  public NewlineToken(Node node) {
    NodeAttributes attributes = new NodeAttributes(node);
		
		indexed = attributes.convertAttributeToInteger("_indexed");
		id = attributes.convertAttributeToInteger("_id");
    refSofa = attributes.convertAttributeToInteger("_ref_sofa");
		begin = attributes.convertAttributeToInteger("begin");
		end = attributes.convertAttributeToInteger("end");
    tokenNumber =attributes.convertAttributeToInteger("tokenNumber");
  }

  public Integer getIndexed() {
    return indexed;
  }

  public Integer getRefSofa() {
    return refSofa;
  }

  public Integer getBegin() {
    return begin;
  }

  public Integer getEnd() {
    return end;
  }

  public Integer getTokenNumber() {
    return tokenNumber;
  }

  @Override
  public Integer getId() {
    return id;
  }

  @Override
  public String getType() {
    return "NewlineToken";
  }
}
