package uk.ac.ed.ecmc.reviewer.annotations;

import java.util.Collections;
import java.util.List;

public class CtakesAnnotation implements Annotation {
	private Integer start;
	private Integer end;
	private String type;
	
	@Override
	public Integer getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	
	@Override
	public Integer getEnd() {
		return end;
	}
	public void setEnd(int end) {
		this.end = end;
	}
	
	@Override
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Override
	public Integer getId() {
		return 0;
	}
	
	@Override
	public List<AdditionalItem> getAdditionalInformation() {
		return Collections.emptyList();
	}
}
