package uk.ac.ed.ecmc.reviewer.files;

import java.io.File;
import java.util.List;

import uk.ac.ed.ecmc.reviewer.annotations.Annotation;
import uk.ac.ed.ecmc.reviewer.medcat.MedCatAnnotationFileReader;

public class MedCatReviewerFile extends AbstractReviewerFile {
    private File underlyingFile;

    public MedCatReviewerFile(File file) {
        underlyingFile = file;
    }

    @Override
    public List<Annotation> getAnnotations() {        
        return MedCatAnnotationFileReader.readFromFile(underlyingFile);
    }
}
