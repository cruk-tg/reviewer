package uk.ac.ed.ecmc.reviewer.files;

import java.util.List;

import uk.ac.ed.ecmc.reviewer.annotations.Annotation;

public abstract class AbstractReviewerFile implements IReviewerFile {
    public abstract List<Annotation> getAnnotations();
}
