package uk.ac.ed.ecmc.reviewer.ctakes;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class NodeAttributes {
  private final NamedNodeMap attributes;

  public NodeAttributes(Node node) {
    attributes = node.getAttributes();
  }

  public Boolean convertAttributeToBoolean(String attributeName) {
		if (null != attributes.getNamedItem(attributeName))
			return Boolean.valueOf(attributes.getNamedItem(attributeName).getTextContent());
		return false;
	}

	public Integer convertAttributeToInteger(String attributeName) {
		if (null != attributes.getNamedItem(attributeName))
			return Integer.valueOf(attributes.getNamedItem(attributeName).getTextContent());
		return Integer.MIN_VALUE;
	}

	public String convertAttributeToString(String attributeName) {
		if (null != attributes.getNamedItem(attributeName))
			return String.valueOf(attributes.getNamedItem(attributeName).getTextContent());
		return "";
	}

	public Double convertAttributeToDouble(String attributeName) {
		if (null != attributes.getNamedItem(attributeName))
			return Double.valueOf(attributes.getNamedItem(attributeName).getTextContent());
		return Double.MIN_VALUE;
	}
}
