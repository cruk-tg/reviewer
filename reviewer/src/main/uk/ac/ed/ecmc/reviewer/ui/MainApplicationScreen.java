package uk.ac.ed.ecmc.reviewer.ui;

import java.util.List;

import com.googlecode.lanterna.input.KeyStroke;

import uk.ac.ed.ecmc.reviewer.annotations.Annotation;
import uk.ac.ed.ecmc.reviewer.files.AbstractReviewerFile;
import uk.ac.ed.ecmc.reviewer.parsers.StringToken;

public class MainApplicationScreen extends ApplicationScreen {
    private final PathologyReportWindow pWindow = new PathologyReportWindow();
	private final NLPDataWindow nlpWindow = new NLPDataWindow();
    private static MainApplicationScreen self = null;

    public static MainApplicationScreen getInstance() {
        if (null == self)
            self = new MainApplicationScreen();

        return self;
    }
    
    private MainApplicationScreen() {
        super();
        this.getTextGUI().addWindow(pWindow);
		this.getTextGUI().addWindow(nlpWindow);
        this.getTextGUI().setActiveWindow(pWindow);
        nlpWindow.setSelectionChangeListener(new SelectionChangeListener() {
			
			@Override
			public void onSelectionChange(Annotation annotation) {
				pWindow.highlightWord(annotation.getStart(), annotation.getEnd()+1);
			}
		});
    }

    public void handleInput(KeyStroke keyStroke) {
        this.getTextGUI().getActiveWindow().handleInput(keyStroke);
    }

    public void setReport(List<StringToken> tokens) {
        pWindow.setReport(tokens);
    }

    public void setNLP(String title, AbstractReviewerFile arf) {
        nlpWindow.setDocument(arf);
        nlpWindow.setTitle(title);
    }
}
