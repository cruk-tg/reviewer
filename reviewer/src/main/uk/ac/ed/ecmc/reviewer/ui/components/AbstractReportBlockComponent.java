package uk.ac.ed.ecmc.reviewer.ui.components;

import com.googlecode.lanterna.bundle.LanternaThemes;
import com.googlecode.lanterna.graphics.Theme;
import com.googlecode.lanterna.gui2.AbstractComponent;
import com.googlecode.lanterna.gui2.Component;

public abstract class AbstractReportBlockComponent<T extends Component> extends AbstractComponent<T> {
	private static Theme HIGHLIGHTED_THEME = LanternaThemes.getRegisteredTheme("blaster");	
    protected Boolean isHighlighted = false;

    @Override
    public Theme getTheme() {
        if (isHighlighted) {
            return HIGHLIGHTED_THEME;
        } else {
            return super.getTheme();
        }
    }
    
    public void setHighlighted(boolean b) {
        isHighlighted = b;      
    }

    public Boolean getHighlighted() {
        return isHighlighted;
    }
    
}
