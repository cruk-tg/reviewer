package uk.ac.ed.ecmc.reviewer.ctakes;

import org.w3c.dom.Node;

public class DiseaseDisorderMention extends AbstractCtakesMention {

	
	public DiseaseDisorderMention(Node node) {
		super(node);
	}

	@Override
	public String getType() {
		return "Disease/Disorder";
	}


}
