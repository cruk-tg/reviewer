package uk.ac.ed.ecmc.reviewer.annotations;

import java.util.Collections;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class MedCatAnnotation  implements Annotation {
	public class Info {
		private String[] snomed;

		public String[] getSnomed() {
			if (null == snomed) {
				return new String[] {};
			} else {
				return snomed;
			}			
		}

		public void setSnomed(String[] snomed) {
			this.snomed = snomed;
		}		
	}
	@SerializedName("pretty_name")
	private String prettyName;
	
	private String cui;
	
	private String tui;
	
	private String type;
	
	@SerializedName("source_value")
	private String sourceValue;
	
	private double acc;
	
	private int start;
	
	private int end;
	
	private int id;

	private Info info;

	public String getPrettyName() {
		return prettyName;
	}

	public String getCui() {
		return cui;
	}

	public String getTui() {
		return tui;
	}

	public String getType() {
		return type;
	}

	public String getSourceValue() {
		return sourceValue;
	}

	public double getAcc() {
		return acc;
	}

	public Integer getStart() {
		return start;
	}

	public Integer getEnd() {
		return end;
	}

	public Integer getId() {
		return id;
	}

	public Info getInfo() {
		return info;
	}

	@Override
	public String toString() {
		return String.format("%s (%d,%d)", getPrettyName(), getStart(), getEnd());
	}

	@Override
	public List<AdditionalItem> getAdditionalInformation() {	
		return Collections.emptyList();
	}
}
