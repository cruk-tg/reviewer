/**
 * 
 */
package uk.ac.ed.ecmc.reviewer.ui;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.googlecode.lanterna.gui2.AbstractTextGUIThread;
import com.googlecode.lanterna.gui2.MultiWindowTextGUI;
import com.googlecode.lanterna.gui2.SameTextGUIThread;
import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.gui2.TextGUIThread.ExceptionHandler;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;

/**
 * @author pmitche2
 *
 */
public class ApplicationScreen {
	private Screen screen = null;
	private static final Logger logger = LogManager.getLogger();
	private WindowBasedTextGUI textGUI;
	private static class ApplicationExceptionHandler implements ExceptionHandler {
		private static final Logger logger = LogManager.getLogger(ApplicationScreen.ApplicationExceptionHandler.class);
		@Override
		public boolean onIOException(IOException e) {
			logger.fatal("An IO exception occurred in the Application GUI thread", e);
			System.exit(-1);
			return true;
		}

		@Override
		public boolean onRuntimeException(RuntimeException e) {
			logger.fatal("A Runtime exception occurred in the Application GUI thread", e);
			System.exit(-1);
			return true;
		}
	}
	
	public ApplicationScreen() {
		logger.debug("Creating new application screen");
		DefaultTerminalFactory terminalFactory = new DefaultTerminalFactory();
		try {
			screen = terminalFactory.createScreen();
			screen.startScreen();
			textGUI = new MultiWindowTextGUI(new SameTextGUIThread.Factory(), screen, new ApplicationWindowManager(screen.getTerminalSize()));
			((AbstractTextGUIThread)textGUI.getGUIThread()).setExceptionHandler(new ApplicationExceptionHandler());
		} catch (IOException e) {
			logger.fatal("Unable to create screen", e);	
			System.exit(1);
		}

	}
	
	public WindowBasedTextGUI getTextGUI() {
		return textGUI;
	}
	
	public KeyStroke pollInput() {
		try {
			return screen.pollInput();
		} catch (IOException e) {
			logger.fatal("Unable to poll input", e);	
			System.exit(1);
			return null;
		}
	}

	public void close() {
		screen.clear();
		try {
			screen.close();
		} catch (IOException e) {
			logger.error("Error closing screen");		
		}
	}
}
