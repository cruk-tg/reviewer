package uk.ac.ed.ecmc.reviewer.ui;

import java.io.File;

import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.gui2.dialogs.MessageDialog;
import com.googlecode.lanterna.gui2.dialogs.MessageDialogButton;
import com.googlecode.lanterna.gui2.menu.Menu;
import com.googlecode.lanterna.gui2.menu.MenuBar;
import com.googlecode.lanterna.gui2.menu.MenuItem;

public class ApplicationMenu {
	private final MenuBar menuBar;
	private final WindowBasedTextGUI textGUI;
	
	public ApplicationMenu(ApplicationScreen applicationScreen) {
		this.menuBar = new MenuBar();
		this.textGUI = applicationScreen.getTextGUI();
		
		addFileMenu();
		addHelpMenu();
	}
	
	public MenuBar getMenuBar() {
		return menuBar;
	}
	
	private void addFileMenu() {
        // "File" menu
        Menu menuFile = new Menu("File");
        menuBar.add(menuFile);
        
        // Menu Item        
        menuFile.add(new MenuItem("Open...", new Runnable() {
            public void run() {
                File file = (new PathologyReportFileDialog(textGUI)).open();
                if (file != null)
                    MessageDialog.showMessageDialog(
                            textGUI, "Open", "Selected file:\n" + file, MessageDialogButton.OK);
            }
        }));
        menuFile.add(new MenuItem("Exit", new Runnable() {
            public void run() {
                System.exit(0);
            }
        }));		
	}
	
	private void addHelpMenu() {
        // "Help" menu
        Menu menuHelp = new Menu("Help");
        menuBar.add(menuHelp);
        menuHelp.add(new MenuItem("Homepage", new Runnable() {
            public void run() {
                MessageDialog.showMessageDialog(
                        textGUI, "Homepage", "https://github.com/mabe02/lanterna", MessageDialogButton.OK);
            }
        }));
        menuHelp.add(new MenuItem("About", new Runnable() {
            public void run() {
                MessageDialog.showMessageDialog(
                        textGUI, "About", "Lanterna drop-down menu", MessageDialogButton.OK);
            }
        }));		
	}
}
