package uk.ac.ed.ecmc.reviewer.ui.components;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.ThemeDefinition;
import com.googlecode.lanterna.gui2.AbstractListBox;
import com.googlecode.lanterna.gui2.ComponentRenderer;
import com.googlecode.lanterna.gui2.TextGUIGraphics;

public class PathologyReportWord extends AbstractReportBlockComponent<PathologyReportWord> {
	ThemeDefinition themeDefinition = getTheme().getDefinition(AbstractListBox.class);
	private final String word;
	private final TerminalSize size;
	
	public PathologyReportWord(String word) {
        this.word = word;
        this.size = new TerminalSize(word.length(), 1);
	}
	
	@Override
	public TerminalSize getSize() {
		return size;
	}
	
	@Override
	protected ComponentRenderer<PathologyReportWord> createDefaultRenderer() {
		return new ComponentRenderer<PathologyReportWord>() {

			@Override
			public TerminalSize getPreferredSize(PathologyReportWord component) {
				return new TerminalSize(word.length(), 1);
			}

			@Override
			public void drawComponent(TextGUIGraphics graphics, PathologyReportWord component) {
				if (isHighlighted) {
					graphics.applyThemeStyle(themeDefinition.getSelected());
				} else {
					graphics.applyThemeStyle(themeDefinition.getNormal());
				}
																			
				graphics.putString(0, 0, word);				
			}			
		};
	}
}
