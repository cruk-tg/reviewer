package uk.ac.ed.ecmc.reviewer.ctakes;

import java.util.HashMap;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class FSArray implements IIdable {
	public class IdOnly implements IIdable {
		private final Integer id;

		public IdOnly(Integer id) {
			this.id = id;
		}

		@Override
		public Integer getId() {
			return id;
		}

		@Override
		public String getType() {			
			return "Integer";
		}
	}
	private static final Logger logger = LogManager.getLogger();
	
	private final HashMap<Integer,IIdable> container = new HashMap<Integer,IIdable>();
	private final Integer id;
	private final Node node;

	public FSArray(Node paramNode) {
		node = paramNode;
		assert(node.getNodeName() == "uima.cas.FSArray");
		NamedNodeMap attributes = node.getAttributes();
		id = Integer.valueOf(attributes.getNamedItem("_id").getTextContent());
		populateList(node);
	}
	
	public HashMap<Integer,IIdable> getContainer() {
		return container;
	}

	public Integer getId() {
		return id;
	}
	
	public String getType() {
		return "FSArray";
	}

	private void populateList(Node node) {
		NodeList nodes = node.getChildNodes();
		for(int i = 0; i < nodes.getLength(); i++) {
			Node n = nodes.item(i);
			String nodeName = n.getNodeName();
			if (nodeName == "i") {
				Integer value = Integer.valueOf(n.getTextContent());
				UmlsConcept umlsConcept = findUmlsConcept(value);
				if (null == umlsConcept) {
					container.put(value, new IdOnly(value));			
				} else {
					container.put(value, umlsConcept);
				}
			}
		}
	}

	private UmlsConcept findUmlsConcept(Integer id) {
		try {
			Node node = findUmlsConceptNode(id);
			if (node != null)
				return new UmlsConcept(node);

		} catch (XPathExpressionException e) {
			logger.fatal("The Umls concept xpath was invalid, exiting");
			logger.fatal(e.getMessage());
			logger.fatal(e.getStackTrace().toString());
			System.exit(1);
		}
		return null;
	}
	private Node findUmlsConceptNode(Integer id) throws XPathExpressionException {
		String xPathExpression = String.format("/CAS/org.apache.ctakes.typesystem.type.refsem.UmlsConcept[@_id='%d']", id);
		NodeList concepts = (NodeList) XPathFactory.newInstance()
																		.newXPath()
																		.compile(xPathExpression)
																		.evaluate(node.getOwnerDocument(), XPathConstants.NODESET);		
		if (concepts.getLength() > 0) {
			return concepts.item(0);
		}

		return null;
	}
}
