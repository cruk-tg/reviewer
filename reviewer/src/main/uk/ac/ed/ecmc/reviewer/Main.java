/**
 * 
 */
package uk.ac.ed.ecmc.reviewer;

/**
 * @author pmitche2
 *
 */
public class Main {
	/**
	 * @param args
	 */
	public static void main(String[] args)  {
		new Application(args).start();
	}

}
