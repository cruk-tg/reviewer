package uk.ac.ed.ecmc.reviewer.readers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import uk.ac.ed.ecmc.reviewer.parsers.StringParser;
import uk.ac.ed.ecmc.reviewer.parsers.StringToken;

public class PathologyReportReader {
	protected static final Logger logger = LogManager.getLogger();	
	private List<StringToken> tokens = new ArrayList<StringToken>();
	private File underlyingFile;

	public PathologyReportReader(File file) {
		underlyingFile = file;
	}

	public void read() {		
        StringBuffer buffer = new StringBuffer();
        try {
            Scanner scanner = new Scanner(underlyingFile);
            while(scanner.hasNextLine()) {
                buffer.append(scanner.nextLine());
                buffer.append("\n");
            }
            scanner.close();								
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage());			
        }
		tokens = StringParser.parse(buffer.toString());
	}

	public List<StringToken> getStringTokens() {
		return tokens;
	}
}
