package uk.ac.ed.ecmc.reviewer.ctakes;

import org.w3c.dom.Node;

public class UmlsConcept implements IIdable {
  
  @Override
  public String getType() {
    return "UmlsConcept";
  }

  public Integer getId() {
    return id;
  }

  public String getCodingScheme() {
    return codingScheme;
  }

  public String getCode() {
    return code;
  }

  public String getOid() {
    return oid;
  }

  public Double getScore() {
    return score;
  }

  public Boolean getDisamiguated() {
    return disamiguated;
  }

  public String getCui() {
    return cui;
  }

  public String getTui() {
    return tui;
  }

  public String getPreferredText() {
    return preferredText;
  }

  protected final Integer id;
  protected final String codingScheme;
  protected final String code;
  protected final String oid;
  protected final Double score;
  protected final Boolean disamiguated;
  protected final String cui;
  protected final String tui;
  protected final String preferredText;

  public UmlsConcept(Node node) {
    NodeAttributes attributes = new NodeAttributes(node);

    id = attributes.convertAttributeToInteger("_id");
    codingScheme = attributes.convertAttributeToString("codingScheme");
    code = attributes.convertAttributeToString("code");
    oid = attributes.convertAttributeToString("oid");
    score = attributes.convertAttributeToDouble("score");
    disamiguated = attributes.convertAttributeToBoolean("disambiguated");
    cui = attributes.convertAttributeToString("cui");
    tui = attributes.convertAttributeToString("tui");
    preferredText = attributes.convertAttributeToString("preferredText");
  }
}
