package uk.ac.ed.ecmc.reviewer.ctakes;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import uk.ac.ed.ecmc.reviewer.annotations.Annotation;

public class CtakesSet extends HashMap<Integer,IIdable>
{  
    public CtakesSet() {
        super();
    }
    public void add(IIdable v) {
        put(v.getId(), v);
    }

    public IIdable find(Integer id) {
        return get(id);
    }

    public List<Annotation> annotations() {	
		return entrySet()
				.stream()
				.filter(x -> x.getValue() instanceof Annotation)
				.map(x -> (Annotation)x.getValue())
				.sorted()
				.collect(Collectors.toList());
	}	
}
