package uk.ac.ed.ecmc.reviewer.parsers;

public class StringToken {
	private final StringTokenType type;
	private final String string;
	private final int start;
	private final int end;
	
	public StringToken(StringTokenType type, String string, int start, int end) {
		this.type = type;
		this.string = string;
		this.start = start;
		this.end = end;
	}
	
	public StringTokenType getType() {
		return type;
	}
	public String getString() {
		return string;
	}
	public int getStart() {
		return start;
	}
	public int getEnd() {
		return end;
	}
	
}
