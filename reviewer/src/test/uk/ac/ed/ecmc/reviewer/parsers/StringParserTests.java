package uk.ac.ed.ecmc.reviewer.parsers;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class StringParserTests {

	@Test
	void testParseWithLetter() {
		List<StringToken> list = StringParser.parse("A");
		assertEquals(1, list.size());		
		assertEquals(StringTokenType.STRING, list.get(0).getType());
		assertEquals("A", list.get(0).getString());
		assertEquals(0, list.get(0).getStart());
		assertEquals(1, list.get(0).getEnd());
	}
	
	@Test
	void testParseWithDigit() {
		List<StringToken> list = StringParser.parse("1");
		assertEquals(1, list.size());		
		assertEquals(StringTokenType.STRING, list.get(0).getType());
		assertEquals("1", list.get(0).getString());
		assertEquals(0, list.get(0).getStart());
		assertEquals(1, list.get(0).getEnd());
	}

	@Test
	void testParseWithPuncutation() {
		List<StringToken> list = StringParser.parse("!");
		assertEquals(1, list.size());		
		assertEquals(StringTokenType.PUNCUTATION, list.get(0).getType());
		assertEquals("!", list.get(0).getString());
		assertEquals(0, list.get(0).getStart());
		assertEquals(1, list.get(0).getEnd());
	}
	
	@Test
	void testParseWithSpace() {
		List<StringToken> list = StringParser.parse(" ");
		assertEquals(1, list.size());		
		assertEquals(StringTokenType.SPACE, list.get(0).getType());
		assertEquals(" ", list.get(0).getString());
		assertEquals(0, list.get(0).getStart());
		assertEquals(1, list.get(0).getEnd());
	}
	
	@Test
	void testParseWithTab() {
		List<StringToken> list = StringParser.parse("\t");
		assertEquals(1, list.size());		
		assertEquals(StringTokenType.SPACE, list.get(0).getType());
		assertEquals("\t", list.get(0).getString());
		assertEquals(0, list.get(0).getStart());
		assertEquals(1, list.get(0).getEnd());
	}
	
	@Test
	void testParseWithNewline() {
		List<StringToken> list = StringParser.parse("\n");
		assertEquals(1, list.size());		
		assertEquals(StringTokenType.NEWLINE, list.get(0).getType());
		assertEquals("\n", list.get(0).getString());
		assertEquals(0, list.get(0).getStart());
		assertEquals(1, list.get(0).getEnd());
	}
	
	@Test
	void testParseWithString() {
		List<StringToken> list = StringParser.parse("Something");
		assertEquals(1, list.size());		
		assertEquals(StringTokenType.STRING, list.get(0).getType());
		assertEquals("Something", list.get(0).getString());
		assertEquals(0, list.get(0).getStart());
		assertEquals(9, list.get(0).getEnd());
	}
	
	@Test
	void testParseWithSentence() {
		List<StringToken> list = StringParser.parse("This is a string.\n");

		assertEquals(9, list.size());		
		assertEquals(StringTokenType.STRING, list.get(0).getType());
		assertEquals("This", list.get(0).getString());
		assertEquals(0, list.get(0).getStart());
		assertEquals(5, list.get(0).getEnd());		
	}
	
	@Test
	void textParseWithSentenceTestSecondWord() {
		List<StringToken> list = StringParser.parse("This is a string.\n");

		assertEquals(9, list.size());		
		assertEquals(StringTokenType.STRING, list.get(2).getType());
		assertEquals("is", list.get(2).getString());
		assertEquals(6, list.get(2).getStart());
		assertEquals(8, list.get(2).getEnd());				
	}
	
	@Test
	void testParseWithSentenceTestList() {
		List<StringToken> list = StringParser.parse("This is a string.\n");
		List<StringTokenType> listOfTypes = new ArrayList<StringTokenType>();
		list.forEach(token -> listOfTypes.add(token.getType()));
		
		List<StringTokenType> compare = Arrays.asList(new StringTokenType[] {StringTokenType.STRING, StringTokenType.SPACE,//This 
				                                                             StringTokenType.STRING, StringTokenType.SPACE,//is
				                                                             StringTokenType.STRING, StringTokenType.SPACE,//a
				                                                             StringTokenType.STRING, StringTokenType.PUNCUTATION, //string.
  			                                                              StringTokenType.NEWLINE});
		assertArrayEquals(compare.toArray(), listOfTypes.toArray());
	}
}
