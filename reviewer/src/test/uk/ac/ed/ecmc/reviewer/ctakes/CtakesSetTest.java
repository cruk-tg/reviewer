package uk.ac.ed.ecmc.reviewer.ctakes;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

public class CtakesSetTest {
    class TestData implements IIdable {
        private Integer id;

        public TestData(Integer id) {
            this.id = id;
        }
        @Override
        public Integer getId() {
            return id;
        }
        @Override
        public String getType() {
            return "Test Type";
        }


        
    }

    @Test
    public void testFind() {
        CtakesSet test = new CtakesSet();
        for(int i=1; i<100; i++) {
            test.add(new TestData(i));
        }
        

        assertEquals(19, test.find(19).getId());
        assertEquals(89, test.find(89).getId());
    }

    @Test
    public void testNotFound() {
        CtakesSet test = new CtakesSet();
        for(int i=1; i<100; i++) {
            if (61 == i) {
                continue; //Do not add 61
            }
            test.add(new TestData(i));
        }
        assertNull(test.find(61), "Should not find 61");
    }
}
